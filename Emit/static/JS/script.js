var contador=1;

function hideGraphicTypes (){
	document.getElementById("opcoesMidia").style.display="block";
	document.getElementById("graphicOptions").style.display="none";
}

function showGraphicTypes(){
	document.getElementById("opcoesMidia").style.display="none";
	document.getElementById("graphicOptions").style.display="block";
}


function escrever(){
	document.getElementById("opcoesMidia").style.display="none";
	document.getElementById("caixatexto").style.display="block";
	}

function canceltext(){
	document.getElementById("opcoesMidia").style.display="block";
	document.getElementById("caixatexto").style.display="none";
}

function cancelgraphic(){
	document.getElementById("opcoesMidia").style.display="block";
	document.getElementById("graphicOptions").style.display="none";
}

function upload(){
		document.getElementById("wissmass").style.display="block";
		document.getElementById("opcoesMidia").style.display="none";
}

function cancelImage(){
	document.getElementById("wissmass").style.display="none";
	document.getElementById("opcoesMidia").style.display="block";
}

function showTable(){
	document.getElementById("opcoesMidia").style.display="none";
	document.getElementById("table").style.display="block";
}

function cancelTable(){
	document.getElementById("opcoesMidia").style.display="block";
	document.getElementById("table").style.display="none";
}

function goBackToMidias(){
	document.getElementById("opcoesMidia").style.display="block";
	document.getElementById("entradasDeValores").style.display="none";
}

function createGraphic(){
	var values = "";
	var labels = "";
	var somatorio = 0;
	var array = new Array(); 
	var result = "";

	for (var i = 0; i< contador; i++) {
		
		var temp1 = document.getElementById("valor_input"+i).value;
		var temp2 = document.getElementById("etiq_input"+i);


		//temp1 = temp1.value;
		temp2 = temp2.value;
		somatorio = somatorio + parseInt(temp1);
		array.push(temp1);
		
		if((values != "") && (labels != "")){
			values += "," + temp1;
			labels += "|" + temp2;
		} else {
			values += temp1;
			labels += temp2;	
		}
	}
	
	for (var j = 0; j< contador; j++) {
		if (j == 0) {
			result = ((array[j]/somatorio)*100).toFixed(1)+"%";
		} else {
			result += "|" + ((array[j]/somatorio)*100).toFixed(1) + "%";
		}

	
	}

	var img = document.createElement("img");
	var graphicType = graphicChecked();
	

	if(graphicType=="Pizza"){
        img.src = "http://chart.apis.google.com/chart?cht=p&chd=t:"+ values + "&chs=380x250&chl="+result+"&chdl="+labels;	
        //pizza 3D
        //img.src = "http://chart.apis.google.com/chart?cht=p3&chd=t:60,40&chs=400x150&chl=Down%20[65%]|Up%20[45%]";
	} else if(graphicType=="Barra"){
		img.src = "http://chart.apis.google.com/chart?cht=bvs&chxt=x,y&chd=t:"+values+"&chco=fa432a&chs=380x250&chxl=0:|" + labels;
	} else {
		 img.src = "http://chart.apis.google.com/chart?cht=lxy&chd=t:,"+ values +"&chxl=0:|"+labels+"&chs=250x100&chxt=x,y";
		//img.src = "http://chart.apis.google.com/chart?cht=lxy:values,"+values+"&chtt=Mean&chs=750x400&chxt=x,x,y";
	}
	var imagem = document.getElementById('panel_body');
	imagem.appendChild(img);
}

function graphicChecked (){
	document.getElementById("graphicOptions").style.display="none";

	var graphic = document.querySelector("input[name='graphicType']:checked");
		switch(graphic.value) {
			case "Pizza":
				document.getElementById("graphicOptions").style.display="none";
				document.getElementById("entradasDeValores").style.display="block";

				/*createGraphic("pizza");
				img.src="http://chart.apis.google.com/chart?cht=bvs&chxt=x,y&chd=t:values,22,26,35,41&chco=fa432a&chs=380x250&chxl=0:|Jan|Fev|Mar|Abr|Mai";*/
			
			return "Pizza";

			case "Barra":
				document.getElementById("graphicOptions").style.display="none";
				document.getElementById("entradasDeValores").style.display="block";

			return "Barra";

			case "Linha":
				document.getElementById("graphicOptions").style.display="none";
				document.getElementById("entradasDeValores").style.display="block";
			return "Linha";
		}
}

function criarPizza(values, labels){
			var img = document.createElement("img");
            img.src = "http://chart.apis.google.com/chart?cht=p&chd=t:"+ values + "&chs=380x250&chl=45%|20%|20%|15%&chdl="+labels;
			// var imagem = document.getElementById('entradasDeValores')
			var imagem = document.getElementById('panel_body');
			imagem.appendChild(img);
}

//ainda nÃ£o foi mexido nessa funÃ§Ã£o
function bars() {
var chart = new CanvasJS.Chart("chartContainer",
{
  title:{
    text: "Top Oil Reserves"    
  },
  animationEnabled: true,
  axisY: {
    title: "Reserves(MMbbl)"
  },
  legend: {
    verticalAlign: "bottom",
    horizontalAlign: "center"
  },
  theme: "theme2",
  data: [

  {        
    type: "column",  
    showInLegend: true, 
    legendMarkerColor: "grey",
    legendText: "MMbbl = one million barrels",
    dataPoints: [      
    {y: 297571, label: "Venezuela"},
    {y: 267017,  label: "Saudi" },
    {y: 175200,  label: "Canada"},
    {y: 154580,  label: "Iran"},
    {y: 116000,  label: "Russia"},
    {y: 97800, label: "UAE"},
    {y: 20682,  label: "US"},        
    {y: 20350,  label: "China"}        
    ]
  }   
  ]
});

chart.render();
}

/*		
<ul >
    <div class="form-group" id="entradas"> 
       <input name="valor_input" class="form-control" type="text" placeholder="Valor" id="valor_input">
    </div>
    <div class="form-group" id="entradas">
       <input name="etiq_input" class="form-control" type="text" placeholder="DescriÃ§Ã£o" id="etiq_input">
    </div>
 </ul>
*/

//Verificar espaÃ§amento entre as caixas de texto
function maisUmaEntrada(){
	var maisUma = document.getElementById("maisEntradas"); //center
	var ul = document.createElement("ul");
	var div1 = document.createElement("div");
	div1.classList.add("form-group");
	div1.id = "entradas";

	var input1 = document.createElement("input");
	input1.classList.add("form-control");
	input1.id = "valor_input"+contador;
	input1.name = "valor_input"+contador;
	input1.placeholder = "Valor";
	input1.type= "number";

	div1.appendChild(input1);

	var div2 = document.createElement("div");
	div2.classList.add("form-group");
	div2.id = "entradas";

	var input2 = document.createElement("input");
	input2.classList.add("form-control");
	input2.id = "etiq_input"+contador;
	input2.name = "etiq_input"+contador;
	input2.type = "text";
	input2.placeholder = "DescriÃ§Ã£o";
	
	div2.appendChild(input2);	

	ul.appendChild(div1);
	ul.appendChild(div2);
	maisUma.appendChild(ul);
	contador++;
}

//criar tabela 

function addRow() {
         
    var myName = document.getElementById("name");
    var age = document.getElementById("age");
    var table = document.getElementById("myTableData");

    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);

    row.insertCell(0).innerHTML= '<input type="button" value = "Delete" onClick="Javacsript:deleteRow(this)">';
    row.insertCell(1).innerHTML= myName.value;
    row.insertCell(2).innerHTML= age.value;

}

function deleteRow(obj) {
     
    var index = obj.parentNode.parentNode.rowIndex;
    var table = document.getElementById("myTableData");
    table.deleteRow(index);
   
}

function load() {
   
    console.log("Page load finished");

}

// 


function criarRelatorio(){

		var grid = document.getElementById("painel");
		var div1 = document.createElement("div");
		div1.classList.add("col-md-3");
		
		var div1 = document.createElement("div");
		
		div1.appendChild(panel);

		var novoTitulo = document.getElementById("titulo-nova");
		var h4 = document.createElement("h4");
		panelHeading.appendChild(h4);
		h4.innerHTML = novoTitulo.value;

		var corpo = document.createElement("div");
		corpo.classList.add("panel-body");
		panel.appendChild(corpo);

		var tCaixinha = document.getElementById("texto-nova");
		corpo.innerHTML = tCaixinha.value;

		row.appendChild(tamCaixinha);

		novoTitulo.value = "";
		tCaixinha.value = "";
	}