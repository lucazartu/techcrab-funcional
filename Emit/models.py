from django.db import models

class Tag(models.Model):
	nome_tag = models.CharField(max_length = 30)
		
	def __str__(self):
		return self.nome_tag


class Relatorio(models.Model):
	tag_ref = models.ForeignKey(Tag)
	titulo_relatorio = models.CharField(max_length = 20)

	def __str__(self):
		return self.titulo_relatorio

class Tabela(models.Model):

	n = models.IntegerField()

class Grafico(models.Model):
	relatorio_ref = models.ForeignKey(Relatorio)
	titulo_grafico = models.CharField(max_length = 100, primary_key=True)
	tipoGrafico = models.CharField(max_length = 40)
	x = models.IntegerField()
	y = models.IntegerField()

	def __str__(self):
		return self.titulo_grafico

class Entrada (models.Model):
	entrada_ref = models.ForeignKey(Grafico)
	value = models.FloatField()
	label = models.CharField(max_length = 40)

	def __str__(self):
		return self.label

class Membro(models.Model):
	primeiro_nome = models.CharField(max_length=30)
	ultimo_nome = models.CharField(max_length=30)
	cargo =  models.CharField(max_length=30)
	email = models.EmailField()
	senha = models.CharField(max_length=30)

	def __str__(self):
		return self.primeiro_nome

class Adm(models.Model):
	admin = models.ForeignKey(Membro)
	
	def __str__(self):
		return self.admin.primeiro_nome

class Usuario(models.Model):
	usr = models.ForeignKey(Membro)

	def __str__(self):
		return self.usr.primeiro_nome

class CaixaDeTexto(models.Model):
	relatorio_linkado = models.ForeignKey(Relatorio)
	texto = models.TextField()
	x = models.IntegerField()
	y = models.IntegerField()

	def __str__(self):
		return self.texto