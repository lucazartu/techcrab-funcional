import os
from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings
from django.template import Context
from django.template.loader import get_template

# def link_callback(uri, rel):
# 	sUrl = settings.STATIC_URL
# 	sRoot = settings.STATIC_ROOT
# 	mUrl = settings.MEDIA_URL
# 	mRoot = settings.MEDIA_ROOT

# 	if uri.startswith(mUrl):
# 		path = os.path.join(mRoot, uri.replace(mUrl, ""))
# 	elif uri.startswith(sUrl):
# 		path = os.path.join(sRoot, uri.replace(sUrl, ""))

# 	if not os.path.isfile(path):
# 		raise Exception(
# 			'media URI must start with %s or %s' % \
# 			(sUrl, mUrl))
# 	return path

# def generate_pdf(request, type):
# 	data = {}
# 	data['today'] = datetime.date.today()
# 	data['farmer'] = 'Old MacDonald'
# 	data['animals'] = [('cow', 'moo'), ('Goat', 'Baa'), ('Pig', 'Oink')]
# 	template = get_template('novoRelatorio.html')
# 	html = template.render(Context(data))
# 	file = open(os.join(settings.MEDIA_ROOT, 'test.pdf'), "w+b")
# 	pisaStatus = pisa.CreatePDF(html, dest=file, 
# 		link_callback = link_callback)
# 	file.seek(0)
# 	pdf = file.read()
# 	file.close()
# 	return HttpResponse(pdf, mimetype='application/pdf')

def home(request):
	template_name = 'relatorios.html'
	return render(request, template_name)