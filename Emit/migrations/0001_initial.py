# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Adm',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='CaixaDeTexto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('texto', models.TextField()),
                ('x', models.IntegerField()),
                ('y', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Entrada',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.FloatField()),
                ('label', models.CharField(max_length=40)),
            ],
        ),
        migrations.CreateModel(
            name='Grafico',
            fields=[
                ('titulo_grafico', models.CharField(max_length=100, serialize=False, primary_key=True)),
                ('tipoGrafico', models.CharField(max_length=40)),
                ('x', models.IntegerField()),
                ('y', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Membro',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('primeiro_nome', models.CharField(max_length=30)),
                ('ultimo_nome', models.CharField(max_length=30)),
                ('cargo', models.CharField(max_length=30)),
                ('email', models.EmailField(max_length=254)),
                ('senha', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Relatorio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo_relatorio', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Tabela',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('n', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome_tag', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('usr', models.ForeignKey(to='Emit.Membro')),
            ],
        ),
        migrations.AddField(
            model_name='relatorio',
            name='tag_ref',
            field=models.ForeignKey(to='Emit.Tag'),
        ),
        migrations.AddField(
            model_name='grafico',
            name='relatorio_ref',
            field=models.ForeignKey(to='Emit.Relatorio'),
        ),
        migrations.AddField(
            model_name='entrada',
            name='entrada_ref',
            field=models.ForeignKey(to='Emit.Grafico'),
        ),
        migrations.AddField(
            model_name='caixadetexto',
            name='relatorio_linkado',
            field=models.ForeignKey(to='Emit.Relatorio'),
        ),
        migrations.AddField(
            model_name='adm',
            name='admin',
            field=models.ForeignKey(to='Emit.Membro'),
        ),
    ]
