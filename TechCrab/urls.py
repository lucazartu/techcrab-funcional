from django.conf.urls import include, url, patterns
from django.contrib import admin
from django.conf import settings	

admin.autodiscover()


urlpatterns = patterns(
	'',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('Emit.urls')),
)